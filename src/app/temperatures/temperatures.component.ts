import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  city; 
  temperature;
  image;
  country;
  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherService.searchWeatherData(this.city).subscribe(
      data => {
        this.temperature = Math.round(data.temperature)+String.fromCharCode(176);
        this.image = data.image;
        this.country = data.country;
      }
    )
  }
}
