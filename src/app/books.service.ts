import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:'hgvfasdfvkljsadhvjasjhvasgvbaslkdjfasdj'},
           {title:'War and Peace', author:'Leo Tolstoy', summary:'hgvfasdfvkljsadhvjasjhvasgvbaslkdjfasdj'},
           {title:'The Magic Mountain', author:'Thomas Mann', summary:'hgvfasdfvkljsadhvjasjhvasgvbaslkdjfasdj'}];

  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one', author:'A new author', summary:'A new summary'}),2000)
  }
  
  
  public getBooks(){
    const booksObservable = new Observable(observer => {
      setInterval(()=> observer.next(this.books),50)
    });
    return booksObservable;
          }



  // public getBooks(){
  //   return this.books;
  // }

 

  constructor() { }
}
